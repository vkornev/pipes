﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UILoadingPanel : MonoBehaviour
{
	public Scrollbar progressBar;

	void Start()
	{
		progressBar.value = 0;
	}

	public void Refresh(float val)
	{
		progressBar.size = val;
	}
}
