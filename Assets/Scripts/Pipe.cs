﻿[System.Serializable]
public enum PipeEnd { TOP, BOTTOM, LEFT, RIGHT, NULL_END };

[System.Serializable]
public enum PipeType { PIPE, START, END, BONUS };

[System.Serializable]
public class Pipe
{
	public PipeEnd[] ends;

	public int GetEndID(PipeEnd end)
	{
		for (int i = 0; i < ends.Length; i++)
			if (ends[i] == end)
				return i;

		return -1;
	}
}
