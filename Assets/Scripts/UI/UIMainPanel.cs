﻿using UnityEngine;
using UnityEngine.UI;

public class UIMainPanel : MonoBehaviour
{
	public Text score;

	void Update()
	{
		score.text = Session.Instance.Score.ToString();
	}

	public void OnStartRandom()
	{
		LoadLevel("level");
	}

	private void LoadLevel(string level)
	{
		Session.Instance.ChangeLevel(level);
	}
}
