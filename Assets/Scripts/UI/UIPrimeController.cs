﻿public class UIPrimeController : Singleton<UIPrimeController>
{
	public UILoadingPanel loadingPanel;
	public UIMainPanel mainPanel;
	public UIGamePanel gamePanel;

	void Start()
	{
		GoMainPanel();
	}

	public void GoLoadingPanel()
	{
		HideAllPanels();

		loadingPanel.gameObject.SetActive(true);
	}

	public void GoMainPanel()
	{
		HideAllPanels();

		mainPanel.gameObject.SetActive(true);
	}

	public void GoGamePanel()
	{
		HideAllPanels();

		gamePanel.gameObject.SetActive(true);
	}

	private void HideAllPanels()
	{
		loadingPanel.gameObject.SetActive(false);
		mainPanel.gameObject.SetActive(false);
		gamePanel.gameObject.SetActive(false);
	}
}
