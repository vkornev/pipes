﻿using UnityEngine;
using System.Collections.Generic;

public class PipeController : MonoBehaviour
{
	public bool IsEmpty
	{
		get { return filledUp <= 0f; }
	}

	public float FilledUp
	{
		get { return filledUp; }
		set { filledUp = value; }
	}

	public bool IsBonusActive
	{
		get { return isBonusActive; }
	}

	public Pipe[] pipes;
	public FillController[] fillCtrls;
	public PipeType type = PipeType.PIPE;

	private float filledUp = 0f;
	private bool isBonusActive = true;

	public bool HasEnd(PipeEnd end)
	{
		for (int i = 0; i < pipes.Length; i++)
			if (pipes[i].GetEndID(end) != -1)
				return true;

		return false;
	}

	public PipeEnd GetOtherEnd(PipeEnd end)
	{
		List<PipeEnd> currentPipeEnds = new List<PipeEnd>();
		for (int i = 0; i < pipes.Length; i++)
		{
			int id = pipes[i].GetEndID(end);
			if (id != -1)
			{
				currentPipeEnds.AddRange(pipes[i].ends);
				currentPipeEnds.Remove(pipes[i].ends[id]);
				return currentPipeEnds[0];
			}
		}

		return PipeEnd.NULL_END;
	}

	public void FillPipe(PipeEnd startFill, float amountFill)
	{
		isBonusActive = false;
		int pipeId = GetPipeIDByEnd(startFill);

		if (pipeId == -1 || fillCtrls.Length == 0) return;
		
		fillCtrls[pipeId].Fill(amountFill > 10f ? 10f : amountFill, pipes[pipeId].ends[0] == startFill);
	}

	public int GetPipeIDByEnd(PipeEnd end)
	{
		for (int i = 0; i < pipes.Length; i++)
			if (pipes[i].GetEndID(end) != -1)
				return i;

		return -1;
	}
}
