﻿using UnityEngine;
using System.Collections;

public class Session : Singleton<Session>
{
    public long Score
    {
        get { return bonusScore + pathScore; }
    }

    public bool IsTouchControl
    {
        get { return isTouchControl; }
        set { isTouchControl = value; }
    }

    public float FlowDelayPart
    {
        get; set;
    }

    private long pathScore = 0;

    private long bonusScore = 0;
    private bool isTouchControl = false;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        InitSession();
    }

    public void UpdatePathScore(long pathScore)
    {
        this.pathScore = pathScore;
    }

    public void UpdateBonusScore(long bonusScore)
    {
        this.bonusScore += bonusScore;
    }

    public void ChangeLevel(string level)
    {
        UIPrimeController.Instance.GoLoadingPanel();
        GameHelper.AdditiveSceneLoad(level, UIPrimeController.Instance.loadingPanel.Refresh, UIPrimeController.Instance.GoGamePanel);
    }

    public void GoBack()
    {
        GameHelper.SceneLoad("main", UIPrimeController.Instance.loadingPanel.Refresh, null);
    }

    private void InitSession()
    {
        bonusScore = 0;
        pathScore = 0;
    }

    private IEnumerator WaitAndFail()
    {
        yield return new WaitForSeconds(2f);
        GoBack();
    }
}
