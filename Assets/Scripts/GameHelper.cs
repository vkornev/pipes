using UnityEngine;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;
using System;

public delegate void UpdateActionDelegate(float updateParameter);
public delegate void ReturnActionDelegate();

/// <summary>
/// Helper class.
/// Contains public methods to ease the development
/// </summary>
public static class GameHelper
{
    public static void SceneLoad(string sceneName, Action<float> updateAction, Action returnAction)
    {
        AsyncSceneLoad(sceneName, updateAction, returnAction, LoadSceneMode.Single);
    }

    public static void AdditiveSceneLoad(string sceneName, Action<float> updateAction, Action returnAction)
    {
        AsyncSceneLoad(sceneName, updateAction, returnAction, LoadSceneMode.Additive);
    }

    private static async void AsyncSceneLoad(string sceneName, Action<float> updateAction, Action returnAction, LoadSceneMode mode)
    {
        AsyncOperation asyncOp = SceneManager.LoadSceneAsync(sceneName, mode);
        asyncOp.completed += (ao) =>
        {
            returnAction?.Invoke();
        };

        //update loading state
        while (!asyncOp.isDone)
        {
            if (updateAction != null) updateAction(asyncOp.progress);
            await Task.Delay(10);
        }
    }
}
