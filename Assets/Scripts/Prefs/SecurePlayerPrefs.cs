﻿using UnityEngine;
using System.Security.Cryptography;
using System.Text;
using System;

public static class SecurePlayerPrefs
{
	private static char[] pass;

	static SecurePlayerPrefs()
	{
		pass = new char[20];

		for (int i = 0; i < 20; i++)
		{
			pass[i] = (char)((i + 3) * 5);
		}
	}

	public static void SetString(string key, string value)
	{
		var desEncryption = new DESEncryption();
		string hashedKey = GenerateMD5(key);
		string encryptedValue = desEncryption.Encrypt(value, new string(pass));
		PlayerPrefs.SetString(hashedKey, encryptedValue);
	}

	public static string GetString(string key)
	{
		string hashedKey = GenerateMD5(key);
		if (PlayerPrefs.HasKey(hashedKey))
		{
			var desEncryption = new DESEncryption();
			string encryptedValue = PlayerPrefs.GetString(hashedKey);
			string decryptedValue;
			desEncryption.TryDecrypt(encryptedValue, new string(pass), out decryptedValue);
			return decryptedValue;
		}
		else
		{
			return PlayerPrefs.GetString(key);
		}
	}

	public static void SetInt(string key, int value)
	{
		SetString(key, Convert.ToString(value));
	}

	public static int GetInt(string key, int defaultValue = 0)
	{
		string value = GetString(key);
		if (0 == value.Length)
		{
			return PlayerPrefs.GetInt(key, defaultValue);
		}

		return Convert.ToInt32(value);
	}

	public static void SetFloat(string key, float value)
	{
		SetString(key, Convert.ToString(value));
	}

	public static float GetFloat(string key, float defaultValue = 0f)
	{
		string value = GetString(key);
		if (0 == value.Length)
		{
			return PlayerPrefs.GetFloat(key, defaultValue);
		}

		return Convert.ToSingle(value);
	}

	public static void SetBool(string key, bool value)
	{
		SetString(key, value ? "true" : "false");
	}

	public static bool GetBool(string key, bool defaultValue = false)
	{
		string value = GetString(key);
		if (0 == value.Length)
		{
			return defaultValue;
		}

		return value.Equals("true");
	}

	public static string GetString(string key, string defaultValue)
	{
		if (HasKey(key))
		{
			return GetString(key);
		}
		else
		{
			return defaultValue;
		}
	}

	public static bool HasKey(string key)
	{
		string hashedKey = GenerateMD5(key);
		bool hasKey = PlayerPrefs.HasKey(hashedKey);

		return hasKey;
	}

	/// <summary>
	/// Generates an MD5 hash of the given text.
	/// WARNING. Not safe for storing passwords
	/// </summary>
	/// <returns>MD5 Hashed string</returns>
	/// <param name="text">The text to hash</param>
	static string GenerateMD5(string text)
	{
		var md5 = new MD5CryptoServiceProvider();
		byte[] inputBytes = Encoding.UTF8.GetBytes(text);
		byte[] hash = md5.ComputeHash(inputBytes);

		// step 2, convert byte array to hex string
		var sb = new StringBuilder();
		for (int i = 0; i < hash.Length; i++)
		{
			sb.Append(hash[i].ToString("X2"));
		}

		return sb.ToString();
	}

	public static void Save()
	{
		PlayerPrefs.Save();
	}
}