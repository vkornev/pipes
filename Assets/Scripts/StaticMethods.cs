using UnityEngine;

public static class StaticMethods
{
    public static void SprayMessage(string pr)
    {
        SprayMessage(pr, null);
    }

    public static void SprayMessage(string pr, object val)
    {
        foreach (Object go in GameObject.FindObjectsOfType(typeof(GameObject)))
        {
            ((GameObject)go).SendMessage(pr, val, SendMessageOptions.DontRequireReceiver);
        }
    }
	
	public static void SetLayerRecursively(GameObject obj, int newLayer)
	{
		obj.layer = newLayer;
		
		foreach (Transform child in obj.transform)
		{
			SetLayerRecursively(child.gameObject, obj.layer);
		}
	}

	public static void SetLayerRecursively(GameObject obj, string newLayer)
	{
		obj.layer = LayerMask.NameToLayer(newLayer);

		foreach (Transform child in obj.transform)
		{
			SetLayerRecursively(child.gameObject, obj.layer);
		}
	}

	public static void SendMessageToChildren<T>(GameObject rootObject, string procedureName, object parameter) where T : Component
	{
		foreach (T obj in rootObject.GetComponentsInChildren<T>())
		{
			obj.SendMessage(procedureName, parameter);
		}
	}
}
