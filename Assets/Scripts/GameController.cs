﻿using UnityEngine;
using System.Collections.Generic;

public class GameController : Singleton<GameController>
{
    public Transform[] pipes;
    public Transform[] starts;
    public Transform[] ends;
    public Transform[] bonuses;
    public LayerMask mask;
    public int pipeQueueLength;

    public float initSpeed = 1f;
    public float flowDelay = 10f;
    public float speedAdd = .01f;
    public int bonusesInLevel = 1;

    private float flowProgress = 0f;
    private float flowDelayCounter = 0f;

    private Dictionary<Vector3, PipeController> pipeDict = new Dictionary<Vector3, PipeController>();
    private List<int> generatedPipeIDs = new List<int>();

    private PipeController startPipe;

    private List<Vector3> path = new List<Vector3>();

    private bool isPlaying = false;
    private long pathScore = 0;

    void Start()
    {
        for (int i = 0; i < pipeQueueLength; i++)
            generatedPipeIDs.Add(Random.Range(0, pipes.Length));

        InitGame();

        GeneratePreview();
    }

    void Update()
    {
        if (!isPlaying) return;

        CheckTouches();

        if (flowDelayCounter >= 0f)
        {
            flowDelayCounter -= Time.deltaTime;
            Session.Instance.FlowDelayPart = flowDelayCounter / flowDelay;
            return;
        }

        flowProgress += initSpeed * Time.deltaTime;

        pathScore = (int)(flowProgress / 10) * 10;
        Session.Instance.UpdatePathScore(pathScore);

        GoFlow();
    }

    private void InitGame()
    {
        bool[,] allPositions = new bool[10, 10];

        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++)
            {
                allPositions[i, j] = true;
            }

        // start
        Vector3 startPos = GetFreeSpot(allPositions, out allPositions);
        Transform t = Instantiate(starts[Random.Range(0, starts.Length)], startPos, Quaternion.identity) as Transform;
        startPipe = t.GetComponent<PipeController>();
        pipeDict.Add(startPos, startPipe);

        // end
        Vector3 endPos = GetFreeSpot(allPositions, out allPositions);
        t = Instantiate(ends[Random.Range(0, starts.Length)], endPos, Quaternion.identity) as Transform;
        PipeController endPipe = t.GetComponent<PipeController>();
        pipeDict.Add(endPos, endPipe);

        // bonus
        for (int i = 0; i < bonusesInLevel; i++)
        {
            Vector3 bonusPos = GetFreeSpot(allPositions, out allPositions);
            t = Instantiate(bonuses[Random.Range(0, bonuses.Length)], bonusPos, Quaternion.identity) as Transform;
            PipeController bonusPipe = t.GetComponent<PipeController>();
            pipeDict.Add(bonusPos, bonusPipe);
        }

        pathScore = 0;
        flowProgress = 0f;
        flowDelayCounter = flowDelay;
        isPlaying = true;
    }

    private Vector3 GetFreeSpot(bool[,] allPositions, out bool[,] updatedPositions)
    {
        updatedPositions = allPositions;

        Vector3 result = Vector3.zero;

        int x = 0, y = 0;

        do
        {
            x = Random.Range(1, 9);
            y = Random.Range(1, 9);
        }
        while (!allPositions[x, y]);

        result = new Vector3(x + .5f, .5f, y + .5f);

        for (int i = x - 1; i <= x + 1; i++)
            for (int j = y - 1; j <= y + 1; j++)
                updatedPositions[i, j] = false;

        return result;
    }

    private void CheckTouches()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask, QueryTriggerInteraction.Collide))
            {
                Vector3 hitPos = hit.point;

                hitPos = new Vector3((int)hitPos.x, 0, (int)hitPos.z);
                hitPos += new Vector3(.5f, .5f, .5f);

                if (pipeDict.ContainsKey(hitPos))
                {
                    if (pipeDict[hitPos].type != PipeType.PIPE || !pipeDict[hitPos].IsEmpty) return;

                    Destroy(pipeDict[hitPos].gameObject);
                    pipeDict.Remove(hitPos);
                }

                Transform t = Instantiate(pipes[generatedPipeIDs[0]], hitPos, Quaternion.identity) as Transform;
                pipeDict.Add(hitPos, t.GetComponent<PipeController>());

                generatedPipeIDs.RemoveAt(0);
                generatedPipeIDs.Add(Random.Range(0, pipes.Length));

                initSpeed += speedAdd;

                GeneratePreview();
            }
        }
    }

    private void GeneratePreview()
    {
        GameObject oldPreview = GameObject.Find("PreviewParent");
        Destroy(oldPreview);

        GameObject previewParent = new GameObject("PreviewParent");

        for (int i = 0; i < generatedPipeIDs.Count; i++)
        {
            Transform t = Instantiate(pipes[generatedPipeIDs[i]], new Vector3(-1f, 0, i * 1.5f + 1f), Quaternion.identity) as Transform;
            t.parent = previewParent.transform;
        }
    }

    private void GoFlow()
    {
        PipeEnd exitPipeEnd = startPipe.pipes[0].ends[0];
        path.Clear();

        Vector3 currentPathPos = startPipe.transform.position;
        path.Add(currentPathPos);

        bool pathGoes = true;
        bool isWin = false;

        float flowCount = flowProgress;

        while (pathGoes && flowCount > 0)
        {
            PipeEnd nextPipeStart = PipeEnd.BOTTOM;
            Vector3 nextPipePos = Vector3.zero;

            switch (exitPipeEnd)
            {
                case PipeEnd.BOTTOM:
                    nextPipeStart = PipeEnd.TOP;
                    nextPipePos = currentPathPos - Vector3.forward;
                    break;
                case PipeEnd.LEFT:
                    nextPipeStart = PipeEnd.RIGHT;
                    nextPipePos = currentPathPos - Vector3.right;
                    break;
                case PipeEnd.RIGHT:
                    nextPipeStart = PipeEnd.LEFT;
                    nextPipePos = currentPathPos + Vector3.right;
                    break;
                case PipeEnd.TOP:
                    nextPipeStart = PipeEnd.BOTTOM;
                    nextPipePos = currentPathPos + Vector3.forward;
                    break;
                default:
                    pathGoes = false;
                    break;
            }

            if (pipeDict.ContainsKey(nextPipePos) && pipeDict[nextPipePos].HasEnd(nextPipeStart))
            {
                if (pipeDict[nextPipePos].type == PipeType.END)
                {
                    pathGoes = false;
                    isWin = true;
                }
                else
                {
                    if (pipeDict[nextPipePos].type == PipeType.BONUS && pipeDict[nextPipePos].IsBonusActive)
                        Session.Instance.UpdateBonusScore(1000);

                    pipeDict[nextPipePos].FillPipe(nextPipeStart, flowCount);
                    flowCount -= 10f;

                    exitPipeEnd = pipeDict[nextPipePos].GetOtherEnd(nextPipeStart);

                    currentPathPos = nextPipePos;
                    path.Add(currentPathPos);
                }
            }
            else
                pathGoes = false;
        }

        if (!pathGoes)
        {
            StaticMethods.SprayMessage("ShowResult", isWin);
            isPlaying = false;
            Session.Instance.UpdatePathScore(pathScore);
        }
    }
}
