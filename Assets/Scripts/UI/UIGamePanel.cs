﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIGamePanel : MonoBehaviour
{
	public Text score;
	public Image resultWidget;
	public Text resultLabel;
	public Image flowDelayBar;

	void Start()
	{
		resultWidget.gameObject.SetActive(false);
	}

	void Update()
	{
		score.text = Session.Instance.Score.ToString();
		flowDelayBar.fillAmount = Session.Instance.FlowDelayPart;
	}

	public void ShowResult(bool isWin)
	{
		resultWidget.gameObject.SetActive(true);
		resultLabel.text = isWin ? "VICTORY!" : "FAIL";
	}

	public void OnExitButton()
	{
		resultWidget.gameObject.SetActive(false);
		SceneManager.UnloadSceneAsync(1);
		Session.Instance.GoBack();
	}
}
