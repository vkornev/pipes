using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	public static T Instance
	{
		get
		{
			if (instance == null)
				instance = FindObjectOfType<T>();

			if (instance == null || !instance.gameObject)
			{
				GameObject instObject = new GameObject(typeof(T).FullName, typeof(T));
				instance = instObject.GetComponent<T>();
				//instance.tag = "Persistent";
			}

			return instance;
		}
	}

	protected static T instance;
}