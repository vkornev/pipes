﻿using UnityEngine;

public class FillController : MonoBehaviour
{
	public Texture2D fillTex;
	public SkinnedMeshRenderer[] parts;

	private float amount = 0;
	private bool direction = true;

	public void Fill(float amount, bool direction)
	{
		if (this.amount >= amount) return;

		this.amount = amount;
		this.direction = direction;

		Refresh();
	}

	private void Refresh()
	{
		if (direction)
			for (int i = 0; i < amount && i < parts.Length; i++)
				parts[i].material.SetTexture("_MainTex", fillTex);
		else
			for (int i = parts.Length - 1; i >= parts.Length - 1 - amount && i >= 0; i--)
				parts[i].material.SetTexture("_MainTex", fillTex);
	}
}
